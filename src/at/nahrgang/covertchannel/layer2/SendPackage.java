package at.nahrgang.covertchannel.layer2;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.jnetpcap.Pcap;
import org.jnetpcap.PcapIf;
import org.jnetpcap.packet.JMemoryPacket;
import org.jnetpcap.packet.JPacket;
import org.jnetpcap.packet.PeeringException;
import org.jnetpcap.protocol.JProtocol;
import org.jnetpcap.protocol.lan.Ethernet;
import org.jnetpcap.protocol.network.Icmp;
import org.jnetpcap.protocol.network.Ip4;
import org.jnetpcap.protocol.tcpip.Tcp;

public class SendPackage {

	public static void main(String[] args) throws PeeringException {
		List<PcapIf> alldevs = new ArrayList<PcapIf>(); // Will be filled with
														// NICs
		StringBuilder errbuf = new StringBuilder(); // For any error msgs

		/***************************************************************************
		 * First get a list of devices on this system
		 **************************************************************************/
		int r = Pcap.findAllDevs(alldevs, errbuf);
		if (r == Pcap.NOT_OK || alldevs.isEmpty()) {
			System.err.printf("Can't read list of devices, error is %s",
					errbuf.toString());
			return;
		} else {
			for (PcapIf devs : alldevs) {
				System.out.println(devs);
			}
		}
		PcapIf device = alldevs.get(2); // Get the right Interface

		//open interface
		int snaplen = 64 * 1024; // Capture all packets, no trucation
		int flags = Pcap.MODE_PROMISCUOUS; // capture all packets
		int timeout = 10 * 1000; // 10 seconds in millis
		Pcap pcap = Pcap.openLive(device.getName(), snaplen, flags, timeout,
				errbuf);
	
		JPacket packet = new JMemoryPacket(4,
				"000012002e48000010306c09c000b2030000" //radio tap header
				+ "d0003a01" //802.11 header
				+ "0016ea47928c" //source mac
				+ "04A151B65A0D"//dest mac
				+ "f88e857d3adf40ec" ); // data
				
		
		/* icmp test echo request */
		/* 		funktioniert 	*/
		/*
		 JPacket packet = new JMemoryPacket(JProtocol.ETHERNET_ID,
				"0016ea47928c" //ethernet header: source mac
				+ "04A151B65A0D "//dest mac"
				+ " 08004500 " //type ip
			    + " 0041a983 40004001 d69a" //ipv4 header
			    + " 0a0000b1 " //source
			    + " 0016ea47 " //dest
			    + "08004d57000100046162636465666768696a6b6c6d6e6f7071727374757677616263646566676869"); //icmp
		 */

		packet.scan(JProtocol.ETHERNET_ID);

		//send data
		if (pcap.sendPacket(packet) != Pcap.OK) {
			System.err.println(pcap.getErr());
		} else {
			System.out.println("ok: " + pcap);
		}
		pcap.close();
	}

}
